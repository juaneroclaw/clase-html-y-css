const SERVER_URL = "http://localhost:8090/garcia/backend-garcia";
console.log(SERVER_URL)
const app = Vue.createApp({
    data() {
        return {
          discografias: [],
          tipos:[],
          bandas:[],
          idBandas:0,
          idTipos:0
        }
      },
    methods: {
        async traerDiscografias() {
            const url = SERVER_URL + "/discografia";
            const response = await fetch(url, {
              headers: {
                'Accept': 'application/json'
              }
            });
            const data = await response.json();
            
            this.discografias = data.CHARLY
            this.discografias.forEach(element => {
              element.ocult = false
            });
          },
        async traerTipos() {
            const url = SERVER_URL + "/tipos";
            const response = await fetch(url, {
              headers: {
                'Accept': 'application/json'
              }
            });
            const data = await response.json();
            
            this.tipos = data.CHARLY
          },
        async traerBandas() {
            const url = SERVER_URL + "/bandas";
            const response = await fetch(url, {
              headers: {
                'Accept': 'application/json'
              }
            });
            const data = await response.json();
            
            this.bandas = data.CHARLY
          },
        async filtrar() {
            const url = SERVER_URL + "/discografias/?idBanda="+this.idBandas+"&idCd="+this.idTipos;
            const response = await fetch(url, {
              headers: {
                'Accept': 'application/json'
              }
            });
            const data = await response.json();
            
            this.discografias = data.CHARLY
            this.discografias.forEach(element => {
              element.ocult = false
            });
          },
        mostrar(index, accion){
         this.discografias[index].ocult = accion
        }
    },
    mounted() {
        this.traerDiscografias();
        this.traerBandas();
        this.traerTipos();

      },
});